import React, { useRef, useEffect, useState } from "react";
import { Form, Row, Button, Table, Col } from "react-bootstrap";
import {
  deleteAuthor,
  postAuthor,
  putAuthor,
  fetchAuthor,
  uploadImage,
} from "../services/Author_sevice";
export default function Author() {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const tempId = useRef("");
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [isUpdate, setIsUpdate] = useState(false);
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const isFirstRun = useRef(true);

  useEffect(() => {
    if (!isFirstRun.current) {
      let pattern3 = /[\w\s]{4,}/g;
      let result3 = pattern3.test(name.trim());
      console.log("this has" + name);
      if (name.trim() === "") {
        setNameError("Author name Cannot be blank!");
      } else if (!result3) {
        setNameError(
          "Author name Cannot contain any special character and atlease 4 character !"
        );
      } else {
        setNameError("");
      }
    }
  }, [name]);
  useEffect(() => {
    if (!isFirstRun.current) {
      let pattern =
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/g;
      let result = pattern.test(email.trim());
      if (result) {
        setEmailError("");
      } else if (email.trim() === "") {
        setEmailError("Email cannot be empty!");
      } else {
        setEmailError("Email is invalid!");
      }
    }
    isFirstRun.current = false;
  }, [email]);

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    let authors = await fetchAuthor();
    setAuthor(authors);
  };

  const onAdd = async (e) => {
    e.preventDefault();
    let tempAuthor = { name: name, email: email };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      tempAuthor.image = url;
    }
    const addNewAuthor = async () => {
      try {
        let response = await postAuthor(tempAuthor);
        alert(response);
        fetch();
      } catch (err) {
        alert(err);
      }
    };
    addNewAuthor();
    setImageFile(null);
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  };

  function onDelete(id) {
    deleteAuthor(id);
    let deletedAuthorArr = author.filter((item) => item._id !== id);
    setAuthor(deletedAuthorArr);
  }

  const onUpdate = async (e) => {
    e.preventDefault();
    let TempAuthor = { name: name, email: email };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      TempAuthor.image = url;
    }
    const updateAuthor = async () => {
      try {
        const response = await putAuthor(tempId.current, TempAuthor);
        alert(response);
        fetch();
      } catch (err) {
        alert(err);
      }
    };
    setIsUpdate(false);
    setImageFile(null);
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
    updateAuthor();
  };

  return (
    <div className="container">
      <h1>Author</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group>
              <Form.Label>Author's Name</Form.Label>
              <Form.Control
                id="name"
                type="text"
                name="name"
                placeholder="Author Name"
                onChange={(event) => {
                  setName(event.target.value);
                }}
              />
              <Form.Text className="text-danger">{nameError}</Form.Text>
            </Form.Group>

            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                id="email"
                type="text"
                name="email"
                placeholder="Email"
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
              ></Form.Control>
              <Form.Text className="text-danger">{emailError}</Form.Text>
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={isUpdate ? onUpdate : onAdd}
            >
              {isUpdate ? "Update" : "Add"}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
          <img id="image" className="w-75" src={imageURL} />
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                label="Choose Image"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {author.map((item, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{item._id}</td>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>
                <img src={item.image} alt="" width={200} height={120} />
              </td>
              <td>
                <Button
                  size="sm"
                  variant="warning"
                  onClick={() => {
                    setIsUpdate(true);
                    tempId.current = item._id;
                    setName(item.name);
                    setEmail(item.email);
                    document.getElementById("name").value = item.name;
                    document.getElementById("email").value = item.email;
                    document.getElementById("image").src = item.image;
                  }}
                >
                  Edit
                </Button>{" "}
                <Button
                  size="sm"
                  variant="danger"
                  onClick={() => onDelete(item._id)}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
