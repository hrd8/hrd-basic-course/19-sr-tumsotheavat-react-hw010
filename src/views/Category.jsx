import React, { useEffect, useState, useRef } from "react";
import {
  fetchCategory,
  deleteCategory,
  postCategory,
  putCategory,
} from "../services/category_service";
import { Table, Button, Form } from "react-bootstrap";
export default function Category() {
  const [categories, setCategories] = useState([]);
  const categoryName = useRef("");
  const [isUpdating, setIsUpdating] = useState(false);
  const tempId = useRef(1);
  useEffect(() => {
    fetch();
  }, []);
  const fetch = async () => {
    let category = await fetchCategory();
    setCategories(category);
  };
  let onDelete = (id) => {
    const deleteCategoryById = async () => {
      try {
        let response = await deleteCategory(id);
        let temp = categories.filter((category) => category._id !== id);
        setCategories(temp);
        alert(response);
      } catch (err) {
        alert(err);
      }
    };
    deleteCategoryById();
  };

  let onAdd = (event) => {
    const addCategoryName = async () => {
      try {
        event.preventDefault();
        let response = await postCategory({ name: categoryName.current });
        document.getElementById("categoryName").value = "";
        alert(response);
        fetch();
      } catch (err) {
        alert(err);
      }
    };
    addCategoryName();
  };

  let onUpdate = (id, event) => {
    const updateCategoryById = async () => {
      try {
        event.preventDefault();
        let response = await putCategory(id, { name: categoryName.current });
        setIsUpdating(false);
        alert(response);
        fetch();
      } catch (err) {
        alert(err);
      }
    };
    updateCategoryById();
  };

  return (
    <div className="container">
      <Form>
        <Form.Group className="mb-3 d-flex" style={{ alignItems: "center" }}>
          <Form.Label style={{ marginRight: "10px", paddingTop: "2px" }}>
            Category
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Category"
            id="categoryName"
            onChange={(event) => {
              categoryName.current = event.target.value;
            }}
          />
          <Button
            size="sm"
            variant="primary"
            style={{ marginLeft: "10px" }}
            type="submit"
            onClick={(event) =>
              isUpdating ? onUpdate(tempId.current, event) : onAdd(event)
            }
          >
            {isUpdating ? "update" : "add"}
          </Button>
        </Form.Group>
      </Form>
      <Table bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category, index) => (
            <tr key={index}>
              <td>{index}</td>
              <td>{category._id}</td>
              <td>{category.name}</td>
              <td>
                <div
                  className="d-flex"
                  style={{
                    justifyContent: "space-evenly",
                    alignItems: "center",
                  }}
                >
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() => {
                      tempId.current = category._id;
                      document.getElementById("categoryName").value =
                        category.name;
                      setIsUpdating(true);
                    }}
                  >
                    Edit
                  </Button>
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(category._id)}
                  >
                    Delete
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}
